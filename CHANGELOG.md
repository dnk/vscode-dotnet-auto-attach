# Changelog

All notable changes to the "vscode-dotnet-auto-attach" extension will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.0.0] - 2018-09-XX

- Initial release

### Added

-
